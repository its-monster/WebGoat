﻿using WebGoatCore.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Threading;

namespace WebGoatCore.Data
{
	public class OrderRepository
	{
		private readonly NorthwindContext _context;
		private readonly CustomerRepository _customerRepository;

		public OrderRepository(NorthwindContext context, CustomerRepository customerRepository)
		{
			_context = context;
			_customerRepository = customerRepository;
		}

		public Order GetOrderById(int orderId)
		{
			return _context.Orders.Single(o => o.OrderId == orderId);
		}

		public int CreateOrder(Order order)
		{
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

			sql = @"
            INSERT INTO Orders (
                CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight,
                ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry
            )
            VALUES (
                @CustomerId, @EmployeeId, @OrderDate, @RequiredDate, @ShippedDate, @ShipVia, @Freight,
                @ShipName, @ShipAddress, @ShipCity, @ShipRegion, @ShipPostalCode, @ShipCountry
            );

            SELECT SCOPE_IDENTITY();";

			using (var command = _context.Database.GetDbConnection().CreateCommand())
			{
				command.CommandText = sql;

				command.Parameters.AddWithValue("@CustomerId", order.CustomerId);
				command.Parameters.AddWithValue("@EmployeeId", order.EmployeeId);
				command.Parameters.AddWithValue("@OrderDate", order.OrderDate);
				command.Parameters.AddWithValue("@RequiredDate", order.RequiredDate);
				command.Parameters.AddWithValue("@ShippedDate", order.ShippedDate);
				command.Parameters.AddWithValue("@ShipVia", order.ShipVia);
				command.Parameters.AddWithValue("@Freight", order.Freight);
				command.Parameters.AddWithValue("@ShipName", order.ShipName);
				command.Parameters.AddWithValue("@ShipAddress", order.ShipAddress);
				command.Parameters.AddWithValue("@ShipCity", order.ShipCity);
				command.Parameters.AddWithValue("@ShipRegion", order.ShipRegion);
				command.Parameters.AddWithValue("@ShipPostalCode", order.ShipPostalCode);
				command.Parameters.AddWithValue("@ShipCountry", order.ShipCountry);

				_context.Database.OpenConnection();

				using var dataReader = command.ExecuteReader();
				dataReader.Read();
				order.OrderId = Convert.ToInt32(dataReader[0]);
			}

			if (order.OrderDetails.Any())
			{
				string sql = @"
                INSERT INTO OrderDetails (OrderId, ProductId, UnitPrice, Quantity, Discount)
                VALUES (@OrderId, @ProductId, @UnitPrice, @Quantity, @Discount)";

				using (var command = _context.Database.GetDbConnection().CreateCommand())
				{
					command.CommandText = sql;

					command.Parameters.Add("@OrderId", SqlDbType.Int);
					command.Parameters.Add("@ProductId", SqlDbType.Int);
					command.Parameters.Add("@UnitPrice", SqlDbType.Decimal);
					command.Parameters.Add("@Quantity", SqlDbType.Int);
					command.Parameters.Add("@Discount", SqlDbType.Decimal);

					_context.Database.OpenConnection();

					foreach (var orderDetail in order.OrderDetails)
					{
						command.Parameters["@OrderId"].Value = order.OrderId;
						command.Parameters["@ProductId"].Value = orderDetail.ProductId;
						command.Parameters["@UnitPrice"].Value = orderDetail.UnitPrice;
						command.Parameters["@Quantity"].Value = orderDetail.Quantity;
						command.Parameters["@Discount"].Value = orderDetail.Discount;

						command.ExecuteNonQuery();
					}
				}
			}

			if (order.Shipment != null)
			{
				string sql = @"
            INSERT INTO Shipments (OrderId, ShipperId, ShipmentDate, TrackingNumber)
            VALUES (@OrderId, @ShipperId, @ShipmentDate, @TrackingNumber)";

				using (var command = _context.Database.GetDbConnection().CreateCommand())
				{
					command.CommandText = sql;

					command.Parameters.AddWithValue("@OrderId", order.OrderId);
					command.Parameters.AddWithValue("@ShipperId", order.Shipment.ShipperId);
					command.Parameters.AddWithValue("@ShipmentDate", order.Shipment.ShipmentDate);
					command.Parameters.AddWithValue("@TrackingNumber", order.Shipment.TrackingNumber);

					_context.Database.OpenConnection();
					command.ExecuteNonQuery();
				}
			}

			return order.OrderId;
		}

		public void CreateOrderPayment(int orderId, decimal amountPaid, string creditCardNumber, DateTime expirationDate, string approvalCode)
		{
			var orderPayment = new OrderPayment()
			{
				AmountPaid = Convert.ToDouble(amountPaid),
				CreditCardNumber = creditCardNumber,
				ApprovalCode = approvalCode,
				ExpirationDate = expirationDate,
				OrderId = orderId,
				PaymentDate = DateTime.Now
			};
			_context.OrderPayments.Add(orderPayment);
			_context.SaveChanges();
		}

		public ICollection<Order> GetAllOrdersByCustomerId(string customerId)
		{
			return _context.Orders
					.Where(o => o.CustomerId == customerId)
					.OrderByDescending(o => o.OrderDate)
					.ThenByDescending(o => o.OrderId)
					.ToList();
		}
	}
}
